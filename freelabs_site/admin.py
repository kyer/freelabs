from django.contrib import admin

import models


@admin.register(models.Lab)
class LabAdmin(admin.ModelAdmin):
    list_display = ['name', 'code']


@admin.register(models.Booking)
class BookingAdmin(admin.ModelAdmin):
    list_display = ['name', 'lab', 'start', 'finish', 'get_day_display']
