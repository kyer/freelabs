from django.shortcuts import render

import models


def index(request):
    return render(request, 'index.html', {
        'best_lab': models.Lab.objects.best(),
        'labs': models.Lab.objects.all()
    })
