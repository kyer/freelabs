from django.db import models

import datetime


class LabManager(models.Manager):
    def best(self):
        """Get the 'best' lab (the one with the most free time)."""
        all_labs = self.all()
        all_labs = [s for s in all_labs if s.current is None]

        # Remove everything that currently has a class.
        # First, look for room with no more bookings for the day. That's ideal.

        free_for_day = [lab for lab in all_labs if lab.upcomming is None]

        if free_for_day:
            return free_for_day[0]

        # If there's nothing, try to find the free lab with the
        # biggest free slot.
        sorted_by_free = sorted(all_labs, key=lambda l: l.upcomming.countdown)
        sorted_by_free = [s for s in sorted_by_free if s.current is None]
        if sorted_by_free:
            return sorted_by_free[0]

        # We can't always get what we want. We get what we need.
        return None


class Lab(models.Model):
    name = models.CharField(
        max_length=50,
        help_text='The lab\'s unique, human-readable name.',
        unique=True
    )

    code = models.CharField(
        max_length=6,
        help_text='The lab\'s access code, if any.',
        blank=True
    )

    @property
    def current(self):
        try:
            current = self.booking_set.filter(
                day=datetime.datetime.now().date().weekday(),
                start__lt=datetime.datetime.now().time(),
                finish__gt=datetime.datetime.now().time()
            )[0]
        except IndexError:
            current = None

        return current

    @property
    def future(self):
        upcomming = self.booking_set.filter(
            day=datetime.datetime.now().date().weekday(),
            start__gt=datetime.datetime.now().time(),
        )
        return upcomming

    @property
    def upcomming(self):
        try:
            return self.future[0]
        except IndexError:
            return None

    objects = LabManager()

    def __str__(self):
        return self.name


class Booking(models.Model):
    MON = 0
    TUE = 1
    WED = 2
    THU = 3
    FRI = 4
    SAT = 5
    SUN = 6

    DAY_OF_WEEK_CHIOCES = (
        (MON, "Monday"),
        (TUE, "Tuesday"),
        (WED, "Wednesday"),
        (THU, "Thursday"),
        (FRI, "Friday"),
        (SAT, "Saturday"),
        (SUN, "Sunday"),
    )

    name = models.CharField(
        max_length=50,
        help_text='The room\'s unique, human-readable name.',
    )

    lab = models.ForeignKey(
        Lab,
        help_text='The lab this booking is for.'
    )

    start = models.TimeField(
        help_text='The time that this booking starts at.'
    )

    finish = models.TimeField(
        help_text='The time that this booking finishes at.'
    )

    day = models.PositiveSmallIntegerField(
        choices=DAY_OF_WEEK_CHIOCES,
        help_text='The day of the week this booking is for.'
    )

    @property
    def datetime_start(self):
        """Assuming booking is running today."""
        return datetime.datetime.combine(datetime.datetime.now().date(), self.start)

    @property
    def datetime_finish(self):
        """Assuming booking is running today."""
        return datetime.datetime.combine(datetime.datetime.now().date(), self.finish)

    @property
    def countdown(self):
        return self.datetime_start - datetime.datetime.now()

    def __str__(self):
        return self.name
