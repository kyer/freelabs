# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('freelabs_site', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='booking',
            name='duration',
        ),
        migrations.AddField(
            model_name='booking',
            name='finish',
            field=models.TimeField(help_text=b'The time that this booking finishes at.'),
            preserve_default=False,
        ),
    ]
