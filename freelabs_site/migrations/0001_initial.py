# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import timedelta.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Booking',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b"The room's unique, human-readable name.", max_length=50)),
                ('start', models.TimeField(help_text=b'The time that this booking starts at.')),
                ('duration', timedelta.fields.TimedeltaField(help_text=b'The duration of the booking.')),
                ('day', models.PositiveSmallIntegerField(help_text=b'The day of the week this booking is for.', choices=[(0, b'Monday'), (1, b'Tuesday'), (2, b'Wednesday'), (3, b'Thursday'), (4, b'Friday'), (5, b'Saturday'), (6, b'Sunday')])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Lab',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b"The lab's unique, human-readable name.", unique=True, max_length=50)),
                ('code', models.CharField(help_text=b"The lab's access code, if any.", max_length=6, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='booking',
            name='lab',
            field=models.ForeignKey(help_text=b'The lab this booking is for.', to='freelabs_site.Lab'),
            preserve_default=True,
        ),
    ]
